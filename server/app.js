console.log("Hello World !");
var x  = 1+1;
console.log(x);

var y;
console.log(y);

var yy = null;
console.log(yy);

var amount = 10;
console.log(amount);
console.log(typeof amount);
amount = true;
console.log(amount);
console.log(typeof amount);


var x = 5;
const x2 = 10;
function A(){
    let x = true;
    let x2 = 100;
    console.log("Inside A " + x);
    console.log(x2);
}

A();
console.log(x);
console.log(x2);

var firstName = "Kenneth";
var lastName = 'Kenneth';
console.log(typeof lastName);

var person = {
    'first name' : 'Ken',
    age : 40,
    gender : 'Male',
    fruits: ['Apple', 'Orange']
}

console.log(person['first name']);
console.log(person.fruits[1]);

var fruits = ['durian', 1, true]

console.log(fruits);

console.log(fruits[1]);

console.log(fruits[4]);

console.log(fruits.length);

console.log(fruits.indexOf(1));
var yy = 'Kenneth';
switch (yy){
    case 'Kenneth':
        console.log("its Ken");
        break;
    case 10:
        console.log("its Aileen");
        break;
    default:
        console.log('everything !');
}

var x = true;
if(x){
    console.log('its x');
}else{
    console.log('not x');
}

function hi1(){
    console.log('hi 1');
}

function hi2(){
    console.log('hi 2');
}

function hiAll(cb1, cb2){
    cb1();
    cb2();
}

//hiAll(hi1, hi2);

setTimeout(function(){
    //hiAll(hi1, hi2);
}, 5000);

console.log("----------------");

var fruits2 = ['durian', 'Apple', 'orange', 'watermelon'];

for(var x = 0; x < fruits2.length; x++){
    console.log(fruits2[x]);
    if(fruits2[x] == 'orange'){
        console.log('EXIT');
        break; // line 106
    }
}


fruits2.forEach(function(value, index){
    console.log(value);
    console.log(index); 
});


fruits2.forEach((value, index)=>{
    console.log(value);
    console.log(index);    
});

fruits2.push('dragonfruit');

console.log(fruits2);

fruits2.pop();

console.log(fruits2);

var result = fruits2.slice(1,3);
console.log('--> ' + fruits2);
console.log(result);

fruits2.splice(1,3, 'jackfruit');
console.log(fruits2);

fruits2.push('durian');
fruits2.push('mangosteen');
console.log(fruits2);

fruits2.shift();
console.log(fruits2);

fruits2.unshift('Banana');
console.log(fruits2);

var numbers = [ 3,4,5,2,7,8 ];

// numbers.sort( (a, b)=>{
//     return a - b;
// });
fruits2.push('banana');
console.log("Before sorting ... " + fruits2);
numbers.sort();
fruits2.sort();
console.log("after sorting ... " + fruits2);

console.log(numbers);
console.log('> ' + numbers.reverse());

const  AA = function AAA(){
    console.log('AAA');
    return 1;
}

function AAAA(){

}
AA();
var returnAA = AA();
console.log(returnAA);
console.log("AA = " + AA);
console.log("AAA = " + AAAA);

var person2 = {
    first_name: 'Kenneth',
    last_name: 'Phang',
    age: 40,
    hello: function(){
        let ic_no = 'FSDFDSF';
        return 'hello kitty';
    }
}

console.log(person2.first_name);
console.log(person2.hello());